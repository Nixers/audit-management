require 'test_helper'

class RecommendationsAuditeesControllerTest < ActionController::TestCase
  setup do
    @recommendations_auditee = recommendations_auditees(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:recommendations_auditees)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create recommendations_auditee" do
    assert_difference('RecommendationsAuditee.count') do
      post :create, recommendations_auditee: { answer: @recommendations_auditee.answer, priority: @recommendations_auditee.priority, severity: @recommendations_auditee.severity, targetdate: @recommendations_auditee.targetdate, title: @recommendations_auditee.title }
    end

    assert_redirected_to recommendations_auditee_path(assigns(:recommendations_auditee))
  end

  test "should show recommendations_auditee" do
    get :show, id: @recommendations_auditee
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @recommendations_auditee
    assert_response :success
  end

  test "should update recommendations_auditee" do
    put :update, id: @recommendations_auditee, recommendations_auditee: { answer: @recommendations_auditee.answer, priority: @recommendations_auditee.priority, severity: @recommendations_auditee.severity, targetdate: @recommendations_auditee.targetdate, title: @recommendations_auditee.title }
    assert_redirected_to recommendations_auditee_path(assigns(:recommendations_auditee))
  end

  test "should destroy recommendations_auditee" do
    assert_difference('RecommendationsAuditee.count', -1) do
      delete :destroy, id: @recommendations_auditee
    end

    assert_redirected_to recommendations_auditees_path
  end
end
