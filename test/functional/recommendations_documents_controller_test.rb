require 'test_helper'

class RecommendationsDocumentsControllerTest < ActionController::TestCase
  setup do
    @recommendations_document = recommendations_documents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:recommendations_documents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create recommendations_document" do
    assert_difference('RecommendationsDocument.count') do
      post :create, recommendations_document: { confidential: @recommendations_document.confidential, desc: @recommendations_document.desc, ref: @recommendations_document.ref, title: @recommendations_document.title, type: @recommendations_document.type, verified: @recommendations_document.verified }
    end

    assert_redirected_to recommendations_document_path(assigns(:recommendations_document))
  end

  test "should show recommendations_document" do
    get :show, id: @recommendations_document
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @recommendations_document
    assert_response :success
  end

  test "should update recommendations_document" do
    put :update, id: @recommendations_document, recommendations_document: { confidential: @recommendations_document.confidential, desc: @recommendations_document.desc, ref: @recommendations_document.ref, title: @recommendations_document.title, type: @recommendations_document.type, verified: @recommendations_document.verified }
    assert_redirected_to recommendations_document_path(assigns(:recommendations_document))
  end

  test "should destroy recommendations_document" do
    assert_difference('RecommendationsDocument.count', -1) do
      delete :destroy, id: @recommendations_document
    end

    assert_redirected_to recommendations_documents_path
  end
end
