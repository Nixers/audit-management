require 'test_helper'

class AuditactionsControllerTest < ActionController::TestCase
  setup do
    @auditaction = auditactions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:auditactions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create auditaction" do
    assert_difference('Auditaction.count') do
      post :create, auditaction: { duedate: @auditaction.duedate, responsible: @auditaction.responsible, status: @auditaction.status, title: @auditaction.title }
    end

    assert_redirected_to auditaction_path(assigns(:auditaction))
  end

  test "should show auditaction" do
    get :show, id: @auditaction
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @auditaction
    assert_response :success
  end

  test "should update auditaction" do
    put :update, id: @auditaction, auditaction: { duedate: @auditaction.duedate, responsible: @auditaction.responsible, status: @auditaction.status, title: @auditaction.title }
    assert_redirected_to auditaction_path(assigns(:auditaction))
  end

  test "should destroy auditaction" do
    assert_difference('Auditaction.count', -1) do
      delete :destroy, id: @auditaction
    end

    assert_redirected_to auditactions_path
  end
end
