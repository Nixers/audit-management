require 'test_helper'

class RecommendationsCompliancesControllerTest < ActionController::TestCase
  setup do
    @recommendations_compliance = recommendations_compliances(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:recommendations_compliances)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create recommendations_compliance" do
    assert_difference('RecommendationsCompliance.count') do
      post :create, recommendations_compliance: { comment: @recommendations_compliance.comment, currentlevel: @recommendations_compliance.currentlevel, noncompliance: @recommendations_compliance.noncompliance, notice: @recommendations_compliance.notice, organization: @recommendations_compliance.organization, targetlevel: @recommendations_compliance.targetlevel, type: @recommendations_compliance.type }
    end

    assert_redirected_to recommendations_compliance_path(assigns(:recommendations_compliance))
  end

  test "should show recommendations_compliance" do
    get :show, id: @recommendations_compliance
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @recommendations_compliance
    assert_response :success
  end

  test "should update recommendations_compliance" do
    put :update, id: @recommendations_compliance, recommendations_compliance: { comment: @recommendations_compliance.comment, currentlevel: @recommendations_compliance.currentlevel, noncompliance: @recommendations_compliance.noncompliance, notice: @recommendations_compliance.notice, organization: @recommendations_compliance.organization, targetlevel: @recommendations_compliance.targetlevel, type: @recommendations_compliance.type }
    assert_redirected_to recommendations_compliance_path(assigns(:recommendations_compliance))
  end

  test "should destroy recommendations_compliance" do
    assert_difference('RecommendationsCompliance.count', -1) do
      delete :destroy, id: @recommendations_compliance
    end

    assert_redirected_to recommendations_compliances_path
  end
end
