require 'test_helper'

class RecommendationsDependenciesControllerTest < ActionController::TestCase
  setup do
    @recommendations_dependency = recommendations_dependencies(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:recommendations_dependencies)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create recommendations_dependency" do
    assert_difference('RecommendationsDependency.count') do
      post :create, recommendations_dependency: { duplicatereco: @recommendations_dependency.duplicatereco, recommendations: @recommendations_dependency.recommendations, title: @recommendations_dependency.title }
    end

    assert_redirected_to recommendations_dependency_path(assigns(:recommendations_dependency))
  end

  test "should show recommendations_dependency" do
    get :show, id: @recommendations_dependency
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @recommendations_dependency
    assert_response :success
  end

  test "should update recommendations_dependency" do
    put :update, id: @recommendations_dependency, recommendations_dependency: { duplicatereco: @recommendations_dependency.duplicatereco, recommendations: @recommendations_dependency.recommendations, title: @recommendations_dependency.title }
    assert_redirected_to recommendations_dependency_path(assigns(:recommendations_dependency))
  end

  test "should destroy recommendations_dependency" do
    assert_difference('RecommendationsDependency.count', -1) do
      delete :destroy, id: @recommendations_dependency
    end

    assert_redirected_to recommendations_dependencies_path
  end
end
