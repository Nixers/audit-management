# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140128054241) do

  create_table "auditactions", :force => true do |t|
    t.string   "title"
    t.string   "status"
    t.string   "duedate"
    t.string   "responsible"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "description"
    t.string   "external_reference"
    t.date     "closed_date"
  end

  create_table "audits", :force => true do |t|
    t.string   "reference"
    t.string   "origin"
    t.date     "startdate"
    t.date     "enddate"
    t.string   "confidentiality"
    t.string   "topic"
    t.string   "standard"
    t.string   "context"
    t.string   "objectives"
    t.string   "scope"
    t.string   "deliverables"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "businessunits", :force => true do |t|
    t.string   "b_name"
    t.string   "b_site"
    t.string   "b_area"
    t.string   "b_city"
    t.string   "b_state"
    t.string   "b_country"
    t.string   "time_zone"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "dashboards", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "documents", :force => true do |t|
    t.string   "title"
    t.string   "location"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "labellings", :force => true do |t|
    t.string   "name"
    t.string   "criteria"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "members", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "policies", :force => true do |t|
    t.string   "p_name"
    t.string   "p_code"
    t.string   "p_scope"
    t.string   "p_objective"
    t.string   "p_description"
    t.integer  "businessunit_id"
    t.integer  "document_id"
    t.integer  "user_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "attach_file_name"
    t.string   "attach_content_type"
    t.integer  "attach_file_size"
    t.datetime "attach_updated_at"
  end

  create_table "recommendations", :force => true do |t|
    t.string   "title"
    t.string   "status"
    t.string   "auditrecommendations"
    t.string   "reference"
    t.string   "primarytopic"
    t.string   "secondarytopic"
    t.string   "compliancetopic"
    t.string   "verification"
    t.string   "priority"
    t.string   "severity"
    t.date     "targetdate"
    t.date     "closeddate"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "recommendations_auditees", :force => true do |t|
    t.string   "title"
    t.string   "answer"
    t.date     "targetdate"
    t.string   "priority"
    t.string   "severity"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "recommendations_compliances", :force => true do |t|
    t.string   "type"
    t.string   "organization"
    t.string   "targetlevel"
    t.string   "currentlevel"
    t.string   "noncompliance"
    t.string   "notice"
    t.string   "comment"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "recommendations_dependencies", :force => true do |t|
    t.string   "title"
    t.string   "recommendations"
    t.string   "duplicatereco"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "recommendations_documents", :force => true do |t|
    t.string   "title"
    t.string   "desc"
    t.string   "ref"
    t.string   "verified"
    t.string   "type"
    t.string   "confidential"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "reports", :force => true do |t|
    t.string   "title"
    t.string   "subtitle"
    t.string   "executivesummary"
    t.string   "source"
    t.date     "duedate"
    t.string   "status"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "audit_id"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
