class CreateRecommendations < ActiveRecord::Migration
  def change
    create_table :recommendations do |t|
      t.string :title
      t.string :status
      t.string :auditrecommendations
      t.string :reference
      t.string :primarytopic
      t.string :secondarytopic
      t.string :compliancetopic
      t.string :verification
      t.string :priority
      t.string :severity
      t.date :targetdate
      t.date :closeddate

      t.timestamps
    end
  end
end
