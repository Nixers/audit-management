class AddClosedDateToAuditactions < ActiveRecord::Migration
  def change
    add_column :auditactions, :closed_date, :date
  end
end
