class CreateRecommendationsCompliances < ActiveRecord::Migration
  def change
    create_table :recommendations_compliances do |t|
      t.string :type
      t.string :organization
      t.string :targetlevel
      t.string :currentlevel
      t.string :noncompliance
      t.string :notice
      t.string :comment

      t.timestamps
    end
  end
end
