class CreateAuditactions < ActiveRecord::Migration
  def change
    create_table :auditactions do |t|
      t.string :title
      t.string :status
      t.string :duedate
      t.string :responsible

      t.timestamps
    end
  end
end
