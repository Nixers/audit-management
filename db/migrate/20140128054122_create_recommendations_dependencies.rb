class CreateRecommendationsDependencies < ActiveRecord::Migration
  def change
    create_table :recommendations_dependencies do |t|
      t.string :title
      t.string :recommendations
      t.string :duplicatereco

      t.timestamps
    end
  end
end
