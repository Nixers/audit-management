class CreateRecommendationsAuditees < ActiveRecord::Migration
  def change
    create_table :recommendations_auditees do |t|
      t.string :title
      t.string :answer
      t.date :targetdate
      t.string :priority
      t.string :severity
      t.timestamps
    end
  end
end
