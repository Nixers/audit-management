class AddExternalReferenceToAuditactions < ActiveRecord::Migration
  def change
    add_column :auditactions, :external_reference, :string
  end
end
