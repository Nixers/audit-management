class CreateRecommendationsDocuments < ActiveRecord::Migration
  def change
    create_table :recommendations_documents do |t|
      t.string :title
      t.string :desc
      t.string :ref
      t.string :verified
      t.string :type
      t.string :confidential

      t.timestamps
    end
  end
end
