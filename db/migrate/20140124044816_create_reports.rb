class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :title
      t.string :subtitle
      t.string :executivesummary
      t.string :source
      t.date :duedate
      t.string :status

      t.timestamps
    end
  end
end
