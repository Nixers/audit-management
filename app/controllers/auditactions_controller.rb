class AuditactionsController < ApplicationController
  # GET /auditactions
  # GET /auditactions.json
  def index
    @auditactions = Auditaction.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @auditactions }
    end
  end

  # GET /auditactions/1
  # GET /auditactions/1.json
  def show
    @auditaction = Auditaction.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @auditaction }
    end
  end

  # GET /auditactions/new
  # GET /auditactions/new.json
  def new
    @auditaction = Auditaction.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @auditaction }
    end
  end

  # GET /auditactions/1/edit
  def edit
    @auditaction = Auditaction.find(params[:id])
  end

  # POST /auditactions
  # POST /auditactions.json
  def create
    @auditaction = Auditaction.new(params[:auditaction])

    respond_to do |format|
      if @auditaction.save
        format.html { redirect_to @auditaction, notice: 'Auditaction was successfully created.' }
        format.json { render json: @auditaction, status: :created, location: @auditaction }
      else
        format.html { render action: "new" }
        format.json { render json: @auditaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /auditactions/1
  # PUT /auditactions/1.json
  def update
    @auditaction = Auditaction.find(params[:id])

    respond_to do |format|
      if @auditaction.update_attributes(params[:auditaction])
        format.html { redirect_to @auditaction, notice: 'Auditaction was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @auditaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /auditactions/1
  # DELETE /auditactions/1.json
  def destroy
    @auditaction = Auditaction.find(params[:id])
    @auditaction.destroy

    respond_to do |format|
      format.html { redirect_to auditactions_url }
      format.json { head :no_content }
    end
  end
end
