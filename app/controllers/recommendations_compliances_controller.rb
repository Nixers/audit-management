class RecommendationsCompliancesController < ApplicationController
  # GET /recommendations_compliances
  # GET /recommendations_compliances.json
  def index
    @recommendations_compliances = RecommendationsCompliance.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @recommendations_compliances }
    end
  end

  # GET /recommendations_compliances/1
  # GET /recommendations_compliances/1.json
  def show
    @recommendations_compliance = RecommendationsCompliance.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @recommendations_compliance }
    end
  end

  # GET /recommendations_compliances/new
  # GET /recommendations_compliances/new.json
  def new
    @recommendations_compliance = RecommendationsCompliance.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @recommendations_compliance }
    end
  end

  # GET /recommendations_compliances/1/edit
  def edit
    @recommendations_compliance = RecommendationsCompliance.find(params[:id])
  end

  # POST /recommendations_compliances
  # POST /recommendations_compliances.json
  def create
    @recommendations_compliance = RecommendationsCompliance.new(params[:recommendations_compliance])

    respond_to do |format|
      if @recommendations_compliance.save
        format.html { redirect_to @recommendations_compliance, notice: 'Recommendations compliance was successfully created.' }
        format.json { render json: @recommendations_compliance, status: :created, location: @recommendations_compliance }
      else
        format.html { render action: "new" }
        format.json { render json: @recommendations_compliance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /recommendations_compliances/1
  # PUT /recommendations_compliances/1.json
  def update
    @recommendations_compliance = RecommendationsCompliance.find(params[:id])

    respond_to do |format|
      if @recommendations_compliance.update_attributes(params[:recommendations_compliance])
        format.html { redirect_to @recommendations_compliance, notice: 'Recommendations compliance was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @recommendations_compliance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recommendations_compliances/1
  # DELETE /recommendations_compliances/1.json
  def destroy
    @recommendations_compliance = RecommendationsCompliance.find(params[:id])
    @recommendations_compliance.destroy

    respond_to do |format|
      format.html { redirect_to recommendations_compliances_url }
      format.json { head :no_content }
    end
  end
end
