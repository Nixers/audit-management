class RecommendationsDocumentsController < ApplicationController
  # GET /recommendations_documents
  # GET /recommendations_documents.json
  def index
    @recommendations_documents = RecommendationsDocument.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @recommendations_documents }
    end
  end

  # GET /recommendations_documents/1
  # GET /recommendations_documents/1.json
  def show
    @recommendations_document = RecommendationsDocument.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @recommendations_document }
    end
  end

  # GET /recommendations_documents/new
  # GET /recommendations_documents/new.json
  def new
    @recommendations_document = RecommendationsDocument.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @recommendations_document }
    end
  end

  # GET /recommendations_documents/1/edit
  def edit
    @recommendations_document = RecommendationsDocument.find(params[:id])
  end

  # POST /recommendations_documents
  # POST /recommendations_documents.json
  def create
    @recommendations_document = RecommendationsDocument.new(params[:recommendations_document])

    respond_to do |format|
      if @recommendations_document.save
        format.html { redirect_to @recommendations_document, notice: 'Recommendations document was successfully created.' }
        format.json { render json: @recommendations_document, status: :created, location: @recommendations_document }
      else
        format.html { render action: "new" }
        format.json { render json: @recommendations_document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /recommendations_documents/1
  # PUT /recommendations_documents/1.json
  def update
    @recommendations_document = RecommendationsDocument.find(params[:id])

    respond_to do |format|
      if @recommendations_document.update_attributes(params[:recommendations_document])
        format.html { redirect_to @recommendations_document, notice: 'Recommendations document was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @recommendations_document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recommendations_documents/1
  # DELETE /recommendations_documents/1.json
  def destroy
    @recommendations_document = RecommendationsDocument.find(params[:id])
    @recommendations_document.destroy

    respond_to do |format|
      format.html { redirect_to recommendations_documents_url }
      format.json { head :no_content }
    end
  end
end
