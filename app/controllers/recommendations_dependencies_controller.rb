class RecommendationsDependenciesController < ApplicationController
  # GET /recommendations_dependencies
  # GET /recommendations_dependencies.json
  def index
    @recommendations_dependencies = RecommendationsDependency.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @recommendations_dependencies }
    end
  end

  # GET /recommendations_dependencies/1
  # GET /recommendations_dependencies/1.json
  def show
    @recommendations_dependency = RecommendationsDependency.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @recommendations_dependency }
    end
  end

  # GET /recommendations_dependencies/new
  # GET /recommendations_dependencies/new.json
  def new
    @recommendations_dependency = RecommendationsDependency.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @recommendations_dependency }
    end
  end

  # GET /recommendations_dependencies/1/edit
  def edit
    @recommendations_dependency = RecommendationsDependency.find(params[:id])
  end

  # POST /recommendations_dependencies
  # POST /recommendations_dependencies.json
  def create
    @recommendations_dependency = RecommendationsDependency.new(params[:recommendations_dependency])

    respond_to do |format|
      if @recommendations_dependency.save
        format.html { redirect_to @recommendations_dependency, notice: 'Recommendations dependency was successfully created.' }
        format.json { render json: @recommendations_dependency, status: :created, location: @recommendations_dependency }
      else
        format.html { render action: "new" }
        format.json { render json: @recommendations_dependency.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /recommendations_dependencies/1
  # PUT /recommendations_dependencies/1.json
  def update
    @recommendations_dependency = RecommendationsDependency.find(params[:id])

    respond_to do |format|
      if @recommendations_dependency.update_attributes(params[:recommendations_dependency])
        format.html { redirect_to @recommendations_dependency, notice: 'Recommendations dependency was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @recommendations_dependency.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recommendations_dependencies/1
  # DELETE /recommendations_dependencies/1.json
  def destroy
    @recommendations_dependency = RecommendationsDependency.find(params[:id])
    @recommendations_dependency.destroy

    respond_to do |format|
      format.html { redirect_to recommendations_dependencies_url }
      format.json { head :no_content }
    end
  end
end
