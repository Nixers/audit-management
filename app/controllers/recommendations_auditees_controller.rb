class RecommendationsAuditeesController < ApplicationController
  # GET /recommendations_auditees
  # GET /recommendations_auditees.json
  def index
    @recommendations_auditees = RecommendationsAuditee.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @recommendations_auditees }
    end
  end

  # GET /recommendations_auditees/1
  # GET /recommendations_auditees/1.json
  def show
    @recommendations_auditee = RecommendationsAuditee.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @recommendations_auditee }
    end
  end

  # GET /recommendations_auditees/new
  # GET /recommendations_auditees/new.json
  def new
    @recommendations_auditee = RecommendationsAuditee.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @recommendations_auditee }
    end
  end

  # GET /recommendations_auditees/1/edit
  def edit
    @recommendations_auditee = RecommendationsAuditee.find(params[:id])
  end

  # POST /recommendations_auditees
  # POST /recommendations_auditees.json
  def create
    @recommendations_auditee = RecommendationsAuditee.new(params[:recommendations_auditee])

    respond_to do |format|
      if @recommendations_auditee.save
        format.html { redirect_to @recommendations_auditee, notice: 'Recommendations auditee was successfully created.' }
        format.json { render json: @recommendations_auditee, status: :created, location: @recommendations_auditee }
      else
        format.html { render action: "new" }
        format.json { render json: @recommendations_auditee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /recommendations_auditees/1
  # PUT /recommendations_auditees/1.json
  def update
    @recommendations_auditee = RecommendationsAuditee.find(params[:id])

    respond_to do |format|
      if @recommendations_auditee.update_attributes(params[:recommendations_auditee])
        format.html { redirect_to @recommendations_auditee, notice: 'Recommendations auditee was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @recommendations_auditee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recommendations_auditees/1
  # DELETE /recommendations_auditees/1.json
  def destroy
    @recommendations_auditee = RecommendationsAuditee.find(params[:id])
    @recommendations_auditee.destroy

    respond_to do |format|
      format.html { redirect_to recommendations_auditees_url }
      format.json { head :no_content }
    end
  end
end
