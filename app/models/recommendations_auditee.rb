class RecommendationsAuditee < ActiveRecord::Base
  attr_accessible :answer, :priority, :severity, :targetdate, :title
end
