class Report < ActiveRecord::Base
  attr_accessible :duedate, :executivesummary, :source, :status, :subtitle, :title, :audit_id
  belongs_to :audit
end
