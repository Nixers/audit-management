class RecommendationsDocument < ActiveRecord::Base
  attr_accessible :confidential, :desc, :ref, :title, :type, :verified
end
