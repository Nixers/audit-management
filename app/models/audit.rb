class Audit < ActiveRecord::Base
  attr_accessible :confidentiality, :context, :deliverables, :enddate, :objectives, :origin, :reference, :scope, :standard, :startdate, :topic
  has_many :reports
end
