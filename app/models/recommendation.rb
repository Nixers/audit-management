class Recommendation < ActiveRecord::Base
  attr_accessible :auditrecommendations, :closeddate, :compliancetopic, :primarytopic, :priority, :reference, :secondarytopic, :severity, :status, :targetdate, :title, :verification
end
