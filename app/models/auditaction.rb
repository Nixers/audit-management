class Auditaction < ActiveRecord::Base
  attr_accessible :duedate, :responsible, :status, :title, :description, :closed_date, :external_reference
end
